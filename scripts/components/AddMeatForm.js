/* Add Meat Form */
import React from 'react';
import autobind from 'autobind-decorator';

@autobind
class AddMeatForm extends React.Component {
  createMeat(event) {
    // 1. Stop form from submitting
    event.preventDefault();
    // 2. Take data from the form and create an object
    const meat = {
      name: this.refs.name.value,
      price: this.refs.price.value,
      status: this.refs.status.value,
      desc: this.refs.desc.value,
      image: this.refs.image.value,
    };
    // 3. Add the fish to the App State
    this.props.addMeat(meat);
    this.refs.meatForm.reset();
  }
  render() {
    return (
      <form className="fish-edit" ref="meatForm" onSubmit={this.createMeat}>
        <input type="text" ref="name" placeholder="Meat Name" />
        <input type="text" ref="price" placeholder="Meat Price" />
        <select ref="status">
          <option value="available">Fresh!</option>
          <option value="unavailable">Sold Out!</option>
        </select>
        <textarea type="text" ref="desc" placeholder="Description"></textarea>
        <input type="text" ref="image" placeholder="URL to Image" />
        <button type="submit">+ Add Item</button>
      </form>
    )
  }
}

export default AddMeatForm;
