/* Inventory */
import React from 'react';
import AddMeatForm from './AddMeatForm';
import autobind from 'autobind-decorator';
import Firebase from 'firebase';
const ref = new Firebase('https://ole-sparkys-bbq.firebaseio.com/')

@autobind
class Inventory extends React.Component {
  constructor() {
    super();
    this.state = {
      uid: ''
    }
  }

  authenticate(provider) {
    console.log('Trying to auth with' + provider);
    ref.authWithOAuthPopup(provider, this.authHandler);
  }

  componentWillMount() {
    var token = localStorage.getItem('token');
    if (token) {
      ref.authWithCustomToken(token, this.authHandler);
    }
  }

  logout() {
    ref.unauth();
    localStorage.removeItem('token');
    this.setState({
      uid: null
    })
  }

  authHandler(err, authData) {
    if (err) {
      console.err(err);
      return;
    }

    // save login token in localStorage
    localStorage.setItem('token', authData.token);

    const storeRef = ref.child(this.props.params.storeId);
    storeRef.on('value', (snapshot) => {
      var data = snapshot.val() || {};

      if (!data.owner) {
        storeRef.set({
          owner: authData.uid
        });
      }
      this.setState({
        uid: authData.uid,
        owner: data.owner || authData.uid
      })
    });

  }

  renderLogin() {
    return (
      <nav className="login">
        <h2>Inventory</h2>
        <p>Login to manage your shops inventory</p>
        <button className="github" onClick={this.authenticate.bind(this, 'github')}>Login with Github</button>
      </nav>
    )
  }
  renderInventory(key) {
    const linkState = this.props.linkState;
    return (
        <div className="fish-edit" key={key}>
          <input type="text" valueLink={linkState(`meats.${key}.name`)}/>
          <input type="text" valueLink={linkState(`meats.${key}.price`)}/>
          <select ref="status" valueLink={linkState(`meats.${key}.status`)}>
            <option value="available">Fresh!</option>
            <option value="unavailable">Sold Out!</option>
          </select>
          <textarea type="text" rows="4" ref="desc" valueLink={linkState(`meats.${key}.desc`)}></textarea>
          <input type="text" valueLink={linkState(`meats.${key}.image`)}/>
          <button onClick={this.props.removeMeat.bind(null, key)}>Remove Meat</button>
        </div>
    );
  }
  render() {
    let logoutButton = <button onClick={this.logout}>Log Out!</button>
    // check if user is logged in
    if (!this.state.uid) {
      return (
        <div>
          { this.renderLogin() }
        </div>
      )
    }

    // if user does not own storeId
    if (this.state.uid !== this.state.owner) {
      return (
        <div>
          <p>Sorry, you aren't the owner of this store.</p>
          {logoutButton}
        </div>
      )
    }
    return (
      <div>
        <h2>Inventory</h2>
        {logoutButton}
        {Object.keys(this.props.meats).map(this.renderInventory)}
        <AddMeatForm {...this.props} />
        <button onClick={this.props.loadSamples}>Load Meat Samples</button>
      </div>
    );
  }
}

Inventory.propTypes = {
  addMeat: React.PropTypes.func.isRequired,
  loadSamples: React.PropTypes.func.isRequired,
  meats: React.PropTypes.object.isRequired,
  linkState: React.PropTypes.func.isRequired,
  removeMeat: React.PropTypes.func.isRequired,
};

export default Inventory;
