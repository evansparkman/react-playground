/* Meat */
import React from 'react';
import helper from '../helpers';
import autobind from 'autobind-decorator';

@autobind
class Meat extends React.Component {
  onButtonClick() {
    this.props.addToOrder(this.props.index);
  }

  render() {
    const details = this.props.details;
    const isAvailable = (details.status === 'available' ? true : false);
    const buttonText = (isAvailable ? 'Add to Order' : 'Sold Out!')
    return (
      <li className="menu-fish">
        <img src={details.image} alt={details.name} />
        <h3 className="fish-name">
          {details.name}
          <span className="price">{helper.formatPrice(details.price)}</span>
        </h3>
        <p>{details.desc}</p>
        <button disabled={!isAvailable} onClick={this.onButtonClick}>{buttonText}</button>
      </li>
    )
  }
}

Meat.propTypes = {
  index: React.PropTypes.string,
  status: React.PropTypes.string
}

export default Meat;
