/* Order */
import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import helper from '../helpers';
import autobind from 'autobind-decorator';

@autobind
class Order extends React.Component {
  renderOrder(key) {
    const meat = this.props.meats[key];
    const count = this.props.order[key];
    const removeButton =
    <button onClick={this.props.removeFromOrder.bind(null, key)}>&times;</button>

    if (!meat) {
      return <li key={key}>Sorry, meat no longer available. {removeButton}</li>
    }
    return (
      <li key={key}>
        <span>
          <CSSTransitionGroup component="span" transitionName="count"
            transitionLeaveTimeout={250} transitionEnterTimeout={250}
            className="count"
          >
            <span key={count}>{count}</span>
          </CSSTransitionGroup>

          lbs {meat.name} {removeButton}
        </span>
        <span className="price">{helper.formatPrice(count * meat.price)}</span>
      </li>
    );
  }

  render() {
    const orderIds = Object.keys(this.props.order);
    const total = orderIds.reduce((prevTotal, key) => {
      const meat = this.props.meats[key];
      const count = this.props.order[key];
      const isAvailable = meat && meat.status === 'available';

      if (meat && isAvailable) {
        return prevTotal + (count * parseInt(meat.price) || 0);
      }

      return prevTotal;
    }, 0);

    return (
      <div className="order-wrap">
        <h2 className="order-title">Your Order</h2>
        <CSSTransitionGroup
          className="order"
          component="ul"
          transitionName="order"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={500}
        >
          {orderIds.map(this.renderOrder)}
          <li className="total">
            <strong>Total:</strong>
            {helper.formatPrice(total)}
          </li>
        </CSSTransitionGroup>

      </div>
    );
  }
}

Order.propTypes = {
  meats: React.PropTypes.object.isRequired,
  order: React.PropTypes.object.isRequired,
  removeFromOrder: React.PropTypes.func.isRequired,
};

export default Order;
