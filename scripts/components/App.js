/* App */
import React from 'react';
import Header from './Header';
import Meat from './Meat';
import Order from './Order';
import Inventory from './Inventory';
import Catalyst from 'react-catalyst';
import reactMixin from 'react-mixin';
import autobind from 'autobind-decorator';

// Firebase
import Rebase from 're-base';
var base = Rebase.createClass('https://ole-sparkys-bbq.firebaseio.com');

@autobind
class App extends React.Component {

  constructor() {
    super();
    this.state = {
      meats : {},
      order : {}
    }
  }

  componentDidMount() {
    base.syncState(this.props.params.storeId + '/meats', {
      context: this,
      state: 'meats'
    });

    const localStorageRef = localStorage.getItem('order-' + this.props.params.storeId);

    if (localStorageRef) {
      this.setState({
        order: JSON.parse(localStorageRef)
      });
    }
  }

  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem('order-' + this.props.params.storeId, JSON.stringify(nextState.order));
  }

  addToOrder(key) {
    this.state.order[key] = this.state.order[key] + 1 || 1;
    this.setState({ order: this.state.order });
  }

  removeFromOrder(key) {
    delete this.state.order[key];
    this.setState({
      order: this.state.order
    });
  }

  addMeat(meat) {
    const timestamp = (new Date()).getTime();
    // update the state object
    this.state.meats['meat-' + timestamp] = meat;
    // set the state object
    this.setState({ meats: this.state.meats });
  }

  removeMeat(key) {
    if (confirm("Are you sure you want to remove this meat?")) {
      this.state.meats[key] = null;
      this.setState({
        meats: this.state.meats
      });
    }
  }

  loadSamples() {
    this.setState({
      meats: require('../sample-meats')
    });
  }

  renderMeat(key) {
    return <Meat key={key} index={key} details={this.state.meats[key]}
      addToOrder={this.addToOrder}/>
  }

  render() {
    return (
      <div className="catch-of-the-day">
        <div className="menu">
          <Header tagline="Barbecue"/>
          <ul className="list-of-fishes">
            {Object.keys(this.state.meats).map(this.renderMeat)}
          </ul>
        </div>
        <Order meats={this.state.meats} order={this.state.order} removeFromOrder={this.removeFromOrder} />
        <Inventory addMeat={this.addMeat} removeMeat={this.removeMeat} loadSamples={this.loadSamples} meats={this.state.meats}
          linkState={this.linkState.bind(this)} {...this.props}/>
      </div>
    )
  }
}

reactMixin.onClass(App, Catalyst.LinkedStateMixin);

export default App;
