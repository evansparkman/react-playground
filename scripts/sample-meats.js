// This is just some sample data so you don't have to think of your own!
module.exports = {
  meat1 : {
    name : 'Smoked Brisket',
    image : 'http://i.istockimg.com/file_thumbview_approve/686019/5/stock-photo-686019-texas-beef-brisket.jpg',
    desc : "It doesn't get much better than Texas Style Smoked Brisket. We can ship full briskets or brisket by the pound.",
    price : 15000,
    status : 'available'
  },

  meat2 : {
    name : 'Pulled Pork',
    image : 'http://i.istockimg.com/file_thumbview_approve/68131513/5/stock-photo-68131513-pulled-pork.jpg',
    desc : "Moist, mouth-watering, savory pulled pork. Ole Sparky's BBQ Pulled Pork will melt in your mouth. Sold by the whole Butt or Shoulder at Market Price or by the pound.",
    price : 1500,
    status : 'available'
  },

  meat3 : {
    name : 'Baby Back Ribs',
    image : 'http://i.istockimg.com/file_thumbview_approve/67095199/5/stock-photo-67095199-homemade-smoked-barbecue-pork-ribs.jpg',
    desc : "Our mouth watering ribs are so good they'll make you wanna slap your Mama. Sold by the Rack or Half Rack.",
    price : 3000,
    status : 'unavailable'
  },

  meat4 : {
    name : 'Chicken',
    image : 'http://i.istockimg.com/file_thumbview_approve/43774970/5/stock-photo-43774970-homemade-grilled-barbecue-chicken.jpg',
    desc : 'Savory and Juciy Chicken Halves and Quarters. ',
    price : 1250 ,
    status : 'available'
  },

  meat5 : {
    name : 'Pork Tenderloin',
    image : 'http://i.istockimg.com/file_thumbview_approve/50294982/5/stock-photo-50294982-homemade-hot-pork-tenderloin.jpg',
    desc : "Sweet with a kick of heat. When we smoke our tenderloin we turn what's traditionally a dry meat and pack it full of juice.",
    price : 1800,
    status : 'available'
  }
};
